<?php

namespace Paladin\Shares\Laravel\Config;

class BaseGlobalConstant
{

    /**
     * 错误调试信息开关
     *
     * @var boolean
     */
    public static $debugMsgSwitch = false;

    /**
     * 错误代码
     *
     * @var array
     */
    public static $errorCode = [
        '000001' => ['error_name' => '系统错误啦，程序猿小哥哥正在火速修复，请君稍安勿躁哦~', 'error_msg' => '系统错误啦，程序猿小哥哥正在火速修复，请君稍安勿躁哦~', 'debug_msg' => '系统错误啦，程序猿小哥哥正在火速修复，请君稍安勿躁哦~', 'is_log' => false, 'is_trace' => false],
        '000002' => ['error_name' => '签名验证失败', 'error_msg' => '签名验证失败', 'debug_msg' => '签名验证失败', 'is_log' => true, 'is_trace' => true],
        '000003' => ['error_name' => '连接超时，请重试。', 'error_msg' => '连接超时，请重试。', 'debug_msg' => '连接超时，请重试。', 'is_log' => false, 'is_trace' => false],
        '000004' => ['error_name' => '微信登录授权失败, 请重新登录', 'error_msg' => '微信登录授权失败, 请重新登录', 'debug_msg' => '微信登录授权失败, 请重新登录', 'is_log' => false, 'is_trace' => false],
        '000005' => ['error_name' => '无效路由, 请检查后重试', 'error_msg' => '无效路由, 请检查后重试', 'debug_msg' => '无效路由, 请检查后重试', 'is_log' => false, 'is_trace' => false],

        '100000' => ['error_name' => '记录错误日志', 'error_msg' => '记录错误日志', 'debug_msg' => '记录错误日志', 'is_log' => true, 'is_trace' => true],
        '100001' => ['error_name' => '非法参数信息', 'error_msg' => '非法参数信息', 'debug_msg' => '非法参数信息', 'is_log' => true, 'is_trace' => true],
        '100002' => ['error_name' => '数据更改失败，联系管理员', 'error_msg' => '数据更改失败，联系管理员', 'debug_msg' => '数据更改失败，联系管理员', 'is_log' => true, 'is_trace' => true],
    ];

    // 0.全局配置
    const ERROR_SYSTEM = '000001'; // 系统错误, 请稍后重试
    const ERROR_SIGN_INVALID = '000002'; // 签名验证失败
    const ERROR_CONNECTION_EXPIRES = '000003'; // 连接超时，请重试。
    const ERROR_WX_LOGIN_AUTH = '000004'; // 微信登录授权失败, 请重新登录
    const ERROR_INVALID_ROUTE = '000005'; // 无效路由, 请检查后重试


    const ERROR_LOG_ERROR = '100000'; // 记录错误日志
    const ERROR_INPUT_INVALID = '100001'; // 非法参数信息
    const ERROR_DB_EXECUTE_FAIL = '100002'; // 数据库操作失败
}
