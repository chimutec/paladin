<?php
namespace Paladin\Shares\Laravel\Services;

use Illuminate\Support\Facades\Cache;
use Paladin\Shares\Laravel\Models\ConfigModel;

class ConfigService
{
    /**
     * 获取配置数据
     *
     * @param string $key 配置项键
     * @return string
     */
    public static function getConfig($key)
    {
        $data = Cache::remember('configs:' . $key, env('CACHE_EXPIRES_TIME_LONG', 10), function () use ($key) {
            $rs = ConfigModel::whereRaw('config_key = ?', [$key])->first();
            return empty($rs) ? '' : $rs['config_value'];
        });
        return $data;
    }

    /**
     * 获取配置数据
     *
     * @date 2015-5-12 上午11:26:01
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @return array
     */
    public static function getConfigs()
    {
        $data = Cache::remember('configs', env('CACHE_EXPIRES_TIME_LONG', 10), function () {
            return ConfigModel::lists('config_value', 'config_key');
        });
        return $data;
    }
}