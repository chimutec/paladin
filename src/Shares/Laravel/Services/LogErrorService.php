<?php
namespace Paladin\Shares\Laravel\Services;

use Paladin\Shares\Laravel\Config\BaseGlobalConstant;
use Paladin\Shares\Laravel\Models\LogError;
use Paladin\Shares\Laravel\Providers\Helper\Helper;
use Illuminate\Support\Facades\Input;

class LogErrorService
{
    /**
     * 返回值标准格式
     *
     * @return array
     */
    public static function construct()
    {
        $jsonData['success'] = true;
        $jsonData['error_id'] = 0;
        $jsonData['error_code'] = '';
        $jsonData['error_msg'] = '';
        $jsonData['debug_msg'] = '';
        return $jsonData;
    }

    /**
     * 记录异常日志
     *
     * @param string $exception 错误信息
     * @param mixed $post 客户提交的数据，默认为空，获取Input::all()
     * @return array
     * @throws \Exception
     */
    public static function logException($exception, $post = '')
    {
        // 报错，回滚所有事务
        // DB::rollback();
        // 数据库记录错误日志
        $errorCode = str_pad($exception->getCode(), 6, '0', STR_PAD_LEFT); // 6位信息编码
        $errorMsg = $exception->getMessage(); // 优先使用自定义消息  异常信息
        $isLog = true; // 是否记录
        $isTrace = true; // 是否追溯异常代码
        $errorData = array();
        $errorData['success'] = false;
        $errorData['error_id'] = 0;
        $errorData['error_code'] = $errorCode;
        $errorData['error_msg'] = $errorMsg;
        $errorData['debug_msg'] = '';

        // 检查是否存在扩展错误消息类, 存在并且有对应错误码, 优先使用扩展类
        $debugMsgSwitch = false;
        if (class_exists('App\\Config\\GlobalConstant') && array_key_exists($errorCode, \App\Config\GlobalConstant::$errorCode)) {
            $mErrorCode = \App\Config\GlobalConstant::$errorCode[$errorCode];
            $debugMsgSwitch = \App\Config\GlobalConstant::$debugMsgSwitch;

        } else {
            $mErrorCode = \Paladin\Shares\Laravel\Config\BaseGlobalConstant::$errorCode[\Paladin\Shares\Laravel\Config\BaseGlobalConstant::ERROR_SYSTEM];
            $debugMsgSwitch = \Paladin\Shares\Laravel\Config\BaseGlobalConstant::$debugMsgSwitch;
            if (array_key_exists($errorCode, \Paladin\Shares\Laravel\Config\BaseGlobalConstant::$errorCode)) {
                $mErrorCode = \Paladin\Shares\Laravel\Config\BaseGlobalConstant::$errorCode[$errorCode];
            }
        }
        if (isset($mErrorCode)) {
            $isTrace = $mErrorCode['is_trace'];
            $isLog = $mErrorCode['is_log'];
            if ($errorData['error_msg'] == '') {
                $errorData['error_msg'] = $mErrorCode['error_msg'];
            }
            if ($debugMsgSwitch) {
                $errorData['debug_msg'] = $mErrorCode['debug_msg'];
            }
        }

        if ($isLog) { // 记录日志
            $data['error_code'] = $errorCode;
            $data['error_msg'] = $errorData['error_msg'];
            $data['trace'] = $isTrace ? $exception->getTraceAsString() : '';
            $data['url'] = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
            $post = $post == '' ? app('request')->all() : $post;
            if (is_array($post)) {
                $data['post'] = json_encode($post);
            } else {
                $data['post'] = $post;
            }
            try {
                // 根据当前项目实例错误日志
                if (class_exists('\\App\\Models\\LogErrorModel')) {
                    $mLogError = new \App\Models\LogErrorModel();
                } elseif (class_exists('\\App\\Models\\TLogError')) {
                    $mLogError = new \App\Models\TLogError();
                }
                $mLogError->create($data);
                $errorData['error_id'] = $mLogError->getKey();
                // 记录调试日志
                Helper::debugLog($errorMsg);
            } catch (\Error $error){
                throw new \Exception($error->getMessage(), $error->getCode());
            }
        }

        return $errorData;
    }

    /**
     * 记录错误编码日志
     *
     * @param string $errorCode 错误编码
     * @param string $message 错误信息
     * @param mixed $post 客户提交的数据，默认为空，获取Input::all()
     * @return array
     * @throws \Exception
     */
    public static function logErrorCode($errorCode, $message = '', $post = '')
    {
        $e = new \Exception($message, $errorCode);
        return self::logException($e, $post);
    }

    /**
     * 返回JSON错误提示
     *
     * @param int $errorCode 错误编码，6位整数
     * @param string $errorMsg 用户错误信息
     * @param string $debugMsg 调试错误信息，默认：''
     * @return array
     */
    public static function logJson($errorCode, $errorMsg = '', $debugMsg = '')
    {
        $jsonData['success'] = false;
        $jsonData['error_id'] = 0;
        $jsonData['error_code'] = trim($errorCode);
        $jsonData['error_msg'] = $errorMsg;
        $jsonData['debug_msg'] = $debugMsg;
        return $jsonData;
    }

    /**
     * 返回输入值JSON错误提示
     *
     * @param string $errorMsg 用户错误信息
     * @param string $debugMsg 调试错误信息，默认：''
     * @return array
     */
    public static function logInvalidValue($errorMsg, $debugMsg = '')
    {
        return self::logJson(BaseGlobalConstant::ERROR_INPUT_INVALID, $errorMsg, $debugMsg);
    }
}