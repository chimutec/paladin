<?php
namespace Paladin\Shares\Laravel\Providers\Api;

use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
	    $this->app->singleton('Api', function($app) {
	        return new Api;
	    });
	}

}
