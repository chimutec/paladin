<?php
namespace Paladin\Shares\Laravel\Providers\Api;

use Exception;
use Illuminate\Support\Facades\Input;
use Paladin\Shares\Laravel\Config\BaseGlobalConstant;
use Paladin\Shares\Laravel\Providers\Helper\Helper;
use Paladin\Shares\Laravel\Services\LogErrorService;

/**
* API接口调用
* @version 1.0.0
*/
class Api {
    /**
     * 会员认证接口
     * 
     * @return string
     */
    public static function getMemberApiUrl()
    {
        return config('app.app_member_api');
    }

    /**
     * 会员认证
     * @param $method
     * @param array $parmas
     * @param string $RequestType
     * @param string $headers
     * @return array
     * @throws Exception
     */
    public static function callInterfaceMember($method, $parmas = [], $RequestType = 'POST', $headers = '')
    {
        return self::callInterfaceCommon(self::getMemberApiUrl(), $method, $parmas, $RequestType, $headers);
    }

    /**
     * 请求接口
     *
     * @param string $url 请求地址
     * @param $method
     * @param array $params 请求参数 json格式
     * @param string $RequestType 请求类型(GET POST PUT DELETE)
     * @param string $headers header头
     * @return array
     * @throws Exception
     */
    public static function callInterfaceCommon($url, $method, $params = [], $RequestType = 'POST', $headers = '')
    {
        $timeout = 30;
        $params['time'] = time();
        if (!isset($params['member_id'])) {
            $params['member_id'] = intval(Input::get('member_id'));
        }
        if (!isset($params['member_token'])) {
            $params['member_token'] = trim(Input::get('member_token'));
        }
        $params = self::_buildRequestPara($params); // 生成要请求给接口的参数数组
        $params_encode = json_encode($params);
        
        //根据请求类型设置特定参数
        $opts = array(
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url.$method,
        );
        if ($headers != "") {
            $opts[CURLOPT_HTTPHEADER] = $headers;
        } else {
            $opts[CURLOPT_HTTPHEADER] = array('Content-type: text/json');
        }
        switch (strtoupper($RequestType)){
            case "GET" : 
                $opts[CURLOPT_HTTPGET] = true;
                $opts[CURLOPT_URL] = $url.$method . '?' . http_build_query($params);
                break;
            case "POST": 
                $opts[CURLOPT_POST] = true;
                $opts[CURLOPT_POSTFIELDS] = $params_encode;
                break;
            case "PUT" : 
                $opts[CURLOPT_CUSTOMREQUEST] = "PUT";
                $opts[CURLOPT_POSTFIELDS] = $params_encode;
                break;
            case "DELETE":
                $opts[CURLOPT_CUSTOMREQUEST] = "DELETE";
                $opts[CURLOPT_POSTFIELDS] = $params_encode;
                break;
            default:
                throw new Exception('不支持的请求方式！');
        }
        /* 初始化并执行curl请求 */
        $ch = curl_init();
        curl_setopt_array($ch, $opts);

        $result = curl_exec($ch);
        curl_close($ch);
        $data = Helper::isJson($result) ? json_decode($result, true) : LogErrorService::logErrorCode(BaseGlobalConstant::ERROR_SYSTEM);
        return $data;
    }

    /**
     * 异步请求接口，不等待返回
     *
     * @param string $url 请求地址
     * @param $method
     * @param array $params 请求参数 json格式
     * @param string $RequestType 请求类型(GET POST PUT DELETE)
     * @param string $headers header头
     *
     * @return void
     */
    public static function postInterfaceCommon($url, $method, $params = [], $RequestType = 'POST', $headers = '')
    {
        $params['time'] = time();
        $params = self::_buildRequestPara($params); // 生成要请求给接口的参数数组
        HttpClient::asynDoPostRequest($url.$method, http_build_query($params));
    }

    /**
    * 生成签名
     *
    * @param string $prestr 已排序要签名字符串
    * @return string
    */
    private static function _buildRequestMysign($prestr)
    {
        $key = config('app.api_interface_key');
        $secrte = config('app.api_interface_secrte');
        return strtoupper(md5($key.$secrte.$prestr)); // $params
    }

    /**
     * 生成要请求给接口的参数数组.
     *
     * @date 2015-11-21下午8:59:37
     *
     * @author lj
     * @param $para_temp 请求前的参数数组
     * @return 排序前的数组
     */
    private static function _buildRequestPara($para_temp) {
        //除去待签名参数数组中的空值和签名参数
        $para_filter = self::_paraFilter($para_temp);
        //对待签名参数数组排序
        $para_sort = self::_argSort($para_filter);
        //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        $prestr = http_build_query($para_sort);
        //生成签名结果
        $mysign = self::_buildRequestMysign($prestr);
        //签名结果与签名方式加入请求提交参数组中
        $para_sort['sign'] = $mysign;
        return $para_sort;
    }

    /**
    * 除去数组中的空值和签名参数
     *
    * @param $para 签名参数组
    * @return 去掉空值与签名参数后的新签名参数组
    */
    private static function _paraFilter($para) {
        $para_filter = [];
        foreach ($para as $key => $val) {
            if ($key != "sign") {
                $para_filter[$key] = $para[$key];
            }
        }
        return $para_filter;
    }

    /**
     * 对数组排序
     *
     * @param $para 排序前的数组
     * @return 排序前的数组
     */
    private static function _argSort($para) {
        ksort($para);
        reset($para);
        return $para;
    }
}
