<?php
namespace Paladin\Shares\Laravel\Providers\Helper;

use App\Models\MemberModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Paladin\Shares\Laravel\Services\ConfigService;

class Helper {
    /**
     * 获取Header数据
     *
     * @param string $header 头信息键
     * @param string $default 默认值，默认空值
     *
     * @date 2015-1-5 下午1:59:11
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function getHeader($header, $default = '')
    {
        if (isset($_SERVER['HTTP_' . strtoupper($header)])) {
            return $_SERVER['HTTP_' . strtoupper($header)];
        }
        if (isset($_REQUEST[$header])) {
            return $_REQUEST[$header];
        }
        return $default;
    }

    /**
     * 获取用户IP地址
     *
     * @date 2014-12-26 上午11:17:58
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function getIp()
    {
        if (isset($_SERVER)) {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $realip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $realip = $_SERVER['HTTP_CLIENT_IP'];
            } else {
                $realip = $_SERVER['REMOTE_ADDR'];
            }
        } else {
            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $realip = getenv('HTTP_X_FORWARDED_FOR');
            } else if (getenv('HTTP_CLIENT_IP')) {
                $realip = getenv('HTTP_CLIENT_IP');
            } else {
                $realip = getenv('REMOTE_ADDR');
            }
        }
        return $realip;
    }

    /**
     * 获取UUID
     *
     * @date 2015-9-8 下午2:51:47
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function getUuid()
    {
        mt_srand(( double )microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        return $charid;
        /*if (function_exists('com_create_guid')) {
            $uuid = trim(com_create_guid(), '{}');
            return $uuid;
        } else {
            mt_srand(( double ) microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);
            $uuid =
            substr($charid, 0, 8) . $hyphen .
            substr($charid, 8, 4) . $hyphen .
            substr($charid, 12, 4) . $hyphen .
            substr($charid, 16, 4) . $hyphen .
            substr($charid, 20, 12) ;
            $uuid = trim($uuid, '{}');
            return $uuid;
        }*/
    }

    /**
     * 获取文件后缀
     *
     * @date 2015-10-22 上午11:35:31
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @param string $fileName 文件名
     * @return string
     */
    public static function getExt($fileName)
    {
        return strtolower(pathinfo($fileName)['extension']);
    }


    /**
     * 手机号码是否在黑名单，或者超过发送限制
     *
     * @date 2015-4-9 上午11:10:49
     * @author: pengyouchuan<cq.peng@qq.com>
     * @param string $mobile 手机号码
     * @return string
     */
    public static function inBlackList($mobile)
    {
        $mobileBlackList = explode(',', ConfigService::getConfig('mobile_black_list'));
        if (in_array($mobile, $mobileBlackList)) {
            return '001013'; // 手机号码已加入黑名单
        }
        if (Cache::tags('blacklist')->has($mobile)) {
            $amount = Cache::tags('blacklist')->increment($mobile);
        } else {
            $amount = 1;
            Cache::tags('blacklist')->put($mobile, $amount, 60);
        }
        if (intval($amount) >= intval(ConfigService::getConfig('mobile_hour_max_num'))) {
            return '001014'; // 超出每小时短信发送限制
        }
        return '';
    }

    /**
     * 判断是否为手机号码格式
     *
     * @date 2015-3-18 上午11:35:00
     * @author: pengyouchuan<cq.peng@qq.com>
     * @param string $mobile 手机号码
     * @return:
     */
    public static function isMobile($mobile)
    {
        return preg_match(ConfigService::getConfig('mobile_preg'), $mobile) ? true : false;
    }

    /**
     * 产生随机字串，可用来自动生成密码
     * 默认长度6位 字母和数字混合 支持中文
     * @param string $len 长度
     * @param string $type 字串类型 0 大小写字母，1 数字，2 大写字母，3 小写字母，4 中文字母，5 渠道已混淆的小写字母+数字，默认 大小写字母+数字
     * @param string $addChars 额外字符
     * @return string
     */
    public static function randString($len = 6, $type = '', $addChars = '')
    {
        $str = '';
        switch ($type) {
            case 0:
                $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . $addChars;
                break;
            case 1:
                $chars = str_repeat('0123456789', 3);
                break;
            case 2:
                $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . $addChars;
                break;
            case 3:
                $chars = 'abcdefghijklmnopqrstuvwxyz' . $addChars;
                break;
            case 4:
                $chars = "们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书术状厂须离再目海交权且儿青才证低越际八试规斯近注办布门铁需走议县兵固除般引齿千胜细影济白格效置推空配刀叶率述今选养德话查差半敌始片施响收华觉备名红续均药标记难存测士身紧液派准斤角降维板许破述技消底床田势端感往神便贺村构照容非搞亚磨族火段算适讲按值美态黄易彪服早班麦削信排台声该击素张密害侯草何树肥继右属市严径螺检左页抗苏显苦英快称坏移约巴材省黑武培著河帝仅针怎植京助升王眼她抓含苗副杂普谈围食射源例致酸旧却充足短划剂宣环落首尺波承粉践府鱼随考刻靠够满夫失包住促枝局菌杆周护岩师举曲春元超负砂封换太模贫减阳扬江析亩木言球朝医校古呢稻宋听唯输滑站另卫字鼓刚写刘微略范供阿块某功套友限项余倒卷创律雨让骨远帮初皮播优占死毒圈伟季训控激找叫云互跟裂粮粒母练塞钢顶策双留误础吸阻故寸盾晚丝女散焊功株亲院冷彻弹错散商视艺灭版烈零室轻血倍缺厘泵察绝富城冲喷壤简否柱李望盘磁雄似困巩益洲脱投送奴侧润盖挥距触星松送获兴独官混纪依未突架宽冬章湿偏纹吃执阀矿寨责熟稳夺硬价努翻奇甲预职评读背协损棉侵灰虽矛厚罗泥辟告卵箱掌氧恩爱停曾溶营终纲孟钱待尽俄缩沙退陈讨奋械载胞幼哪剥迫旋征槽倒握担仍呀鲜吧卡粗介钻逐弱脚怕盐末阴丰雾冠丙街莱贝辐肠付吉渗瑞惊顿挤秒悬姆烂森糖圣凹陶词迟蚕亿矩康遵牧遭幅园腔订香肉弟屋敏恢忘编印蜂急拿扩伤飞露核缘游振操央伍域甚迅辉异序免纸夜乡久隶缸夹念兰映沟乙吗儒杀汽磷艰晶插埃燃欢铁补咱芽永瓦倾阵碳演威附牙芽永瓦斜灌欧献顺猪洋腐请透司危括脉宜笑若尾束壮暴企菜穗楚汉愈绿拖牛份染既秋遍锻玉夏疗尖殖井费州访吹荣铜沿替滚客召旱悟刺脑措贯藏敢令隙炉壳硫煤迎铸粘探临薄旬善福纵择礼愿伏残雷延烟句纯渐耕跑泽慢栽鲁赤繁境潮横掉锥希池败船假亮谓托伙哲怀割摆贡呈劲财仪沉炼麻罪祖息车穿货销齐鼠抽画饲龙库守筑房歌寒喜哥洗蚀废纳腹乎录镜妇恶脂庄擦险赞钟摇典柄辩竹谷卖乱虚桥奥伯赶垂途额壁网截野遗静谋弄挂课镇妄盛耐援扎虑键归符庆聚绕摩忙舞遇索顾胶羊湖钉仁音迹碎伸灯避泛亡答勇频皇柳哈揭甘诺概宪浓岛袭谁洪谢炮浇斑讯懂灵蛋闭孩释乳巨徒私银伊景坦累匀霉杜乐勒隔弯绩招绍胡呼痛峰零柴簧午跳居尚丁秦稍追梁折耗碱殊岗挖氏刃剧堆赫荷胸衡勤膜篇登驻案刊秧缓凸役剪川雪链渔啦脸户洛孢勃盟买杨宗焦赛旗滤硅炭股坐蒸凝竟陷枪黎救冒暗洞犯筒您宋弧爆谬涂味津臂障褐陆啊健尊豆拔莫抵桑坡缝警挑污冰柬嘴啥饭塑寄赵喊垫丹渡耳刨虎笔稀昆浪萨茶滴浅拥穴覆伦娘吨浸袖珠雌妈紫戏塔锤震岁貌洁剖牢锋疑霸闪埔猛诉刷狠忽灾闹乔唐漏闻沈熔氯荒茎男凡抢像浆旁玻亦忠唱蒙予纷捕锁尤乘乌智淡允叛畜俘摸锈扫毕璃宝芯爷鉴秘净蒋钙肩腾枯抛轨堂拌爸循诱祝励肯酒绳穷塘燥泡袋朗喂铝软渠颗惯贸粪综墙趋彼届墨碍启逆卸航衣孙龄岭骗休借" . $addChars;
                break;
            case 5:
                // 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
                $chars = 'abcdefghijkmnpqrstuvwxyz23456789' . $addChars;
                break;
            case 6:
                $chars = 'ACDEFHJKLMNPQSTUVWXYZ' . $addChars; //去掉 B G R I O   0 1
                break;
            default :
                // 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
                $chars = 'ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789' . $addChars;
                break;
        }
        if ($len > 10) {//位数过长重复字符串一定次数
            $chars = $type == 1 ? str_repeat($chars, $len) : str_repeat($chars, 5);
        }
        if ($type != 4) {
            $chars = str_shuffle($chars);
            $str = substr($chars, 0, $len);
        } else {
            // 中文随机字
            for ($i = 0; $i < $len; $i++) {
                $str .= self::msubstr($chars, floor(mt_rand(0, mb_strlen($chars, 'utf-8') - 1)), 1, 'utf-8', false);
            }
        }
        return $str;
    }

    /**
     * 生成一定数量的随机数，并且不重复
     * @param integer $number 数量
     * @param string $len 长度
     * @param string $type 字串类型
     * 0 字母 1 数字 其它 混合
     * @return string
     */
    public static function buildCountRand($number, $length = 4, $mode = 1)
    {
        if ($mode == 1 && $length < strlen($number)) {
            //不足以生成一定数量的不重复数字
            return false;
        }
        $rand = array();
        for ($i = 0; $i < $number; $i++) {
            $rand[] = self::randString($length, $mode);
        }
        $unqiue = array_unique($rand);
        if (count($unqiue) == count($rand)) {
            return $rand;
        }
        $count = count($rand) - count($unqiue);
        for ($i = 0; $i < $count * 3; $i++) {
            $rand[] = self::randString($length, $mode);
        }
        $rand = array_slice(array_unique($rand), 0, $number);
        return $rand;
    }

    /**
     *  带格式生成随机字符 支持批量生成
     *  但可能存在重复
     * @param string $format 字符格式
     *     # 表示数字 * 表示字母和数字 $ 表示字母
     * @param integer $number 生成数量
     * @return string | array
     */
    public static function buildFormatRand($format, $number = 1)
    {
        $str = array();
        $length = strlen($format);
        for ($j = 0; $j < $number; $j++) {
            $strtemp = '';
            for ($i = 0; $i < $length; $i++) {
                $char = substr($format, $i, 1);
                switch ($char) {
                    case "*"://字母和数字混合
                        $strtemp .= self::randString(1);
                        break;
                    case "#"://数字
                        $strtemp .= self::randString(1, 1);
                        break;
                    case "$"://大写字母
                        $strtemp .= self::randString(1, 2);
                        break;
                    default://其他格式均不转换
                        $strtemp .= $char;
                        break;
                }
            }
            $str[] = $strtemp;
        }
        return $number == 1 ? $strtemp : $str;
    }

    /**
     * 获取一定范围内的随机数字 位数不足补零
     * @param integer $min 最小值
     * @param integer $max 最大值
     * @return string
     */
    public static function randNumber($min, $max)
    {
        return sprintf("%0" . strlen($max) . "d", mt_rand($min, $max));
    }

    /**
     * 根据当前时间生成18位随机数
     * YMDHIS+RAND4
     *
     * @date 2014-9-24 下午3:51:45
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function randNumber18()
    {
        return date("YmdHis") . self::randString(4, 1);
    }

    /**
     * 随机生成16位字符串
     * @date 16-01-22 下午2:44
     * @author yangzhao<406297616@qq.com>
     * @return
     */
    public static function randNumber16()
    {
        return self::randString(16, 2, '0123456789');
    }

    /**
     * 随机生成16位 提货卡字符串  去掉 B G R I O 0 1
     * @date 16-01-22 下午2:44
     * @author yangzhao<406297616@qq.com>
     * @return
     */
    public static function randGiftNumber16()
    {
        return self::randString(16, 6, '23456789');
    }

    /**
     * 根据当前时间生成32位随机数
     * YMDHIS+RAND18
     *
     * @date 2014-9-24 下午3:51:45
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function randNumber32()
    {
        return date("YmdHis") . self::randString(18, 1);
    }

    /**
     * 加密password
     *
     * @date 2015-9-8 下午2:15:35
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function passwordEnCode($password)
    {
        return md5($password);
    }

    /**
     * 设置Token
     *
     * @date 2014-8-19 下午1:47:46
     * @author: pengyouchuan<cq.peng@qq.com>
     * @param string $token token key
     * @param mixed $data token value
     * @param int $expires 过期时间 默认0
     * @return:
     */
    public static function putToken($token, $data, $expires = 0)
    {
        $expires = empty($expires) ? ConfigService::getConfig('token_expires_time') : $expires;
        if (empty($expires)) {
            return Cache::tags('token')->forever($token, $data);
        } else {
            return Cache::tags('token')->put($token, $data, intval($expires));
        }
    }

    /**
     * 删除Token
     *
     * @date 2015-9-10 上午11:59:26
     * @author: pengyouchuan<cq.peng@qq.com>
     * @param string $token token key
     * @return:
     */
    public static function forgetToken($token)
    {
        return Cache::tags('token')->forget($token);
    }

    /**
     * 刷新Token过期时间
     *
     * @date 2014-8-19 下午12:06:01
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function refreshToken($expires = '')
    {
        $expires = empty($expires) ? ConfigService::getConfig('token_expires_time') : $expires;
        $token = Config::get('token');
        if (empty($token)) {
            return false;
        }
        $tokenData = Cache::tags('token')->get($token);
        if (empty($tokenData)) {
            return false;
        }
        Cache::tags('token')->put($token, $tokenData, $expires);
        return true;
    }

    /**
     * 验证用户Token是否合法
     *
     * @date 2014-8-18 下午5:56:28
     * @author pengyouchuan<cq.peng@qq.com>
     * @param int $memberId 用户id，默认空
     * @return array [memberId, token]
     */
    public static function checkToken($member_id = '')
    {
        if (ConfigService::getConfig('token_enable') == 'false') {
            return ['member_id' => $member_id, 'token' => ''];
        }
        $token = Config::get('token');
        if (empty($token)) {
            header('HTTP/1.1 401 Unauthorized');
            exit;
        }
        $tokenData = Cache::tags('token')->get($token);
        if (!empty($tokenData)) {
            if ($member_id == '' || $member_id == $tokenData['member_id']) {
                return $tokenData;
            }
        }
        header('HTTP/1.1 401 Unauthorized');
        exit;
    }

    /**
     * 根据Token获取用户ID
     *
     * @date 2014-8-18 下午5:56:28
     * @author: pengyouchuan<cq.peng@qq.com>
     * @param string $token token
     * @return: int 用户ID
     */
    public static function getMemberIdByToken($token)
    {
        $tokenData = Cache::tags('token')->get($token);
        if (!empty($tokenData)) {
            return $tokenData['member_id'];
        }
        return 0;
    }

    /**
     * 通过原图生成三种不同大小的图片
     *
     * @date 2015-9-10 下午5:04:45
     * @author: pengyouchuan<cq.peng@qq.com>
     * @param string $orignal 图片地址
     * @param bool $resize 是否重设大小，默认true，否则只返回图片路径
     * @return: bool | array
     */
    public static function make3Pic($orignal, $resize = true)
    {
        $ext = self::getExt($orignal);
        $uploadPath = dirname($orignal);
        $fileName = basename($orignal, '.' . $ext);
        $data['L'] = $imgPathL = $uploadPath . '/' . $fileName . '-L.' . $ext;
        $data['M'] = $imgPathM = $uploadPath . '/' . $fileName . '-M.' . $ext;
        $data['S'] = $imgPathS = $uploadPath . '/' . $fileName . '-S.' . $ext;
        if ($resize) {
            $quality = ConfigService::getConfig('pic_default_quality'); // 图片质量
            list($lsize, $msize, $ssize) = explode(',', ConfigService::getConfig('pic_3pic_size')); // 图片三个尺寸
            $orignalImage = Image::make($orignal);
            $imgLImage = Image::make($orignal)->resize($lsize, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $imgMImage = Image::make($orignal)->resize($msize, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $imgSImage = Image::make($orignal)->resize($ssize, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $orignalImage->save($orignal, $quality);
            $imgLImage->save($imgPathL, $quality);
            $imgMImage->save($imgPathM, $quality);
            $imgSImage->save($imgPathS, $quality);
            if (!file_exists($imgPathL) || !file_exists($imgPathM) || !file_exists($imgPathS)) return false;
        }
        return $data;
    }

    /**
     * 裁剪图片
     *
     * @param string $orignal 图片地址
     * @param integer $width 宽
     * @param integer $height 高
     * @param boolean $ratio 是否按比例裁剪
     * @param boolean $upsize Determines whether the image can be upsized
     * @return string
     */
    public static function makeResize($orignal, $width = null, $height = null, $ratio = false, $upsize = true)
    {
        if ($ratio == false) {
            $ratio = function ($constraint) {
                $constraint->aspectRatio();
            };
        }
        $quality = ConfigService::getConfig('pic_default_quality'); // 图片质量
        $imgImage = Image::make($orignal)->resize($width, $height, $ratio, $upsize);
        $check = $imgImage->save($orignal, $quality);
        if (!file_exists($orignal)) return false;
        return $orignal;
    }

    /**
     * 完整的URL
     *
     * @date 2014-8-25 上午9:11:36
     * @author: pengyouchuan<cq.peng@qq.com>
     * @return:
     */
    public static function fullPath($url, $key, $default = '')
    {
        if (empty($url)) return $default;
        $url = trim($url, '/');
        if (strpos($url, '://') === false) $url = trim(ConfigService::getConfig($key), '/') . '/' . $url;
        return $url;
    }

    /**
     * 完整的图片地址
     *
     * @date 2014-8-25 上午9:11:36
     * @author pengyouchuan<cq.peng@qq.com>
     * @return string
     */
    public static function fullPicPath($url)
    {
        if (empty($url)) {
            return '';
        }
        return self::fullPath($url, 'pic_url_root');//, $default
    }

    /**
     * 完整媒体文件地址
     *
     * @param $url
     * @param string $default
     * @return string
     */
    public static function fullMediaPath($url, $default = '')
    {
        return self::fullPath($url, 'media_url_root', $default);
    }

    /**
     * 裁切七牛云图片
     *
     * @param string $imgUrl 图片链接
     * @param integer $width 宽
     * @param integer $height 高
     * @param boolean $crop 是否正中裁切
     * @return string
     */
    public static function imageResize($imgUrl, $width, $height = 0, $crop = true)
    {

        if (preg_match("/(crm.cdn.unionblue.cn|pav0uky18.bkt.clouddn.com|ihomejoy-jiangjin.leinpay.com|ihomejoy-test.leinpay.com)/", $imgUrl)) {
            if (FALSE === strpos($imgUrl, "?imageMogr2")) {
                $imgUrl .= "?imageMogr2";
            }
            if ($width && $height) {
                $imgUrl .= "/thumbnail/!" . $width . "x" . $height . "r";
                if ($crop) {
                    $imgUrl .= "/gravity/Center/crop/" . $width . "x" . $height;
                }
            } else if ($width) {
                $imgUrl .= "/thumbnail/" . $width . "x>";
            } else {
                $imgUrl .= "/thumbnail/x" . $height . ">";
            }
        }
        return $imgUrl;
    }

    /**
     * 日期转换成时间
     *
     * @date 2015-10-13 上午11:14:41
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @param mixed $date string|carbon 时间字符串或者carbon对象
     * @param bool $fill 是否填充0，增加三位毫秒数
     * @return int 时间戳
     */
    public static function dateToTime($date, $fill = false)
    {
        $time = strtotime($date);
        return $time;
    }

    /**
     * 计算任务完成率
     *
     * @date 2015-10-23 上午9:56:33
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @param int $completeNum 完成数
     * @param int $taskNum 任务总数
     * @return int
     */
    public static function getCompleteRate($completeNum, $taskNum)
    {
        $rate = floor($completeNum / $taskNum * 100); // 完成率向下取整
        if ($rate == 0 && $completeNum > 0) return 1; // 最小1
        return $rate > 100 ? 100 : $rate;
    }

    /**
     * 任务url
     *
     * @param int $taskId 任务id
     * @param string $url 任务url
     *
     * @return string
     */
    public static function getTaskUrl($taskId, $url = '')
    {
        $url = trim($url, '/');
        return $url == '' ? self::fullOpenPath(AppConfigService::getConfig('task_view_url') . '?taskId=' . $taskId) : $url;
    }

    /**
     * 微信/终端绑定 url验证
     *
     * r是一个随机的uuid值，作为验证加密的一部分。值v的规则由4部分合在一起然后做计算：
     * ‘uid后三位’+‘r’的前两位 + 固定字符串 CMSoaKvugdzJ5XBvEEaqcYhcVaejDtkD然后做MD5 32位加密（大写）。
     *
     * @param $uid 第三方注册平台的用户uid
     * @param $r 随机uuid值（长度64位以下）
     *
     *
     * @return string
     */
    public static function urlVerification($uid, $r)
    {
        $str = substr($uid, -3) . substr($r, 0, 2) . AppConfigService::getConfig('app_secret');

        return strtoupper(self::passwordEnCode($str));
    }

    /**
     * 封装自己的数据库自增，使用update方法更新。
     *
     * @date 2015-10-27 上午10:42:57
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @param array $updates 更新数组
     * @param string $field 更新字段
     * @param int $amount 数量，默认1
     * @return array
     */
    public static function modelIncrement(&$updates, $field, $amount = 1)
    {
        $updates[$field] = DB::raw("`$field` + $amount");
        return $updates;
    }

    /**
     * 封装自己的数据库自减，使用update方法更新。
     *
     * @date 2015-10-27 上午10:42:57
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @param array $updates 更新数组
     * @param string $field 更新字段
     * @param int $amount 数量，默认1
     * @return array
     */
    public static function modelDecrement(&$updates, $field, $amount = 1)
    {
        $updates[$field] = DB::raw("`$field` - $amount");
        return $updates;
    }

    /**
     * 记录调试信息
     *
     * @date 2015-11-5 上午10:32:45
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @param string $name 调试标题
     * @param string $msg 调试信息，默认空
     * @param bool $input 是否记录请求信息，默认 true
     * @return bool
     */
    public static function debugLog($name, $msg = '', $input = true)
    {
        $debug = Config::get('app.debug');
        if ($debug) {
            if ($msg != '') $msg = "\n" . $msg;
            Log::debug($name . ':' . $msg . self::debugInput());
            return true;
        }
        return false;
    }

    /**
     * 格式化调试输入参数
     *
     * @date 2015-11-5 上午9:35:29
     *
     * @author pengyouchuan<cq.peng@qq.com>
     * @return
     */
    public static function debugInput()
    {
        $result['SERVER'] = $_SERVER;
        $result['REQUEST'] = $_REQUEST;
        $result['INPUT'] = Input::all();
        return "\n" . json_encode($result);
    }

    /**
     * 计算评价比率
     *
     * @param int $num 评论数量
     * @param int $total 评论总数
     * @return int
     */
    public static function commentRate($num, $total)
    {
        if ($total == 0 || $num == 0) return 0;
        return intval($num / $total * 100);
    }

    /**
     * 库存剩余数量
     *
     * @param int $stock 库存量
     * @param int $rate 库存比率
     * @param int $sale 销售量
     * @return int
     */
    public static function surplusNum($stock, $rate, $sale)
    {
        $surplusNum = intval($stock * $rate / 100) - intval($sale);
        return $surplusNum >= 0 ? $surplusNum : 0;
    }

    /**
     * 数组排序
     * @param $arr
     * @return mixed
     */
    public static function arraySort(&$arr)
    {
        ksort($arr);
        reset($arr);
        return $arr;
    }

    /**
     * 多维数组排序
     * array_sort($test, 'count', 'desc');
     */
    public static function array_sort($arr, $keys, $type = 'asc')
    {
        $keysvalue = $new_array = array();
        foreach ($arr as $k => $v) {
            $keysvalue[$k] = $v[$keys];
        }
        if ($type == 'asc') {
            asort($keysvalue);
        } else {
            arsort($keysvalue);
        }
        reset($keysvalue);
        foreach ($keysvalue as $k => $v) {
            $new_array[$k] = $arr[$k];
        }
        return $new_array;
    }

    /**
     * 格式化好的树
     */
    public static function genTree($items)
    {
        $tree = array();
        foreach ($items as $item)
            if (isset($items[$item['parent_id']]))
                $items[$item['parent_id']]['son'][] = &$items[$item['id']];
            else
                if ($item['parent_id'] == 0 || in_array($item['parent_id'], $items)) {
                    $tree[] = &$items[$item['id']];
                }
        return $tree;
    }

    /**
     * 根据树 生成option
     */
    public static function getTreeData($tree, $level = 0, $levelRelative = 0, $parent_level = -1)
    {
        static $html = '';
        foreach ($tree as $t) {
            $selected = $t['selected'] == 1 ? 'selected="selected"' : '';
            $disabled = ($parent_level == -1 || $parent_level == $t['level']) ? '' : 'disabled="disabled"';
            $html .= '<option value="' . $t['id'] . '" level="' . $level . '" ' . $selected . $disabled . '>' . str_repeat("&nbsp;", $levelRelative * 4) . $t['name'] . '</option>';
            if (isset($t['son'])) {
                self::getTreeData($t['son'], $level + 1, $levelRelative + 1, $parent_level);
            }
        }
        return $html;
    }

    public static function setKeyWithColumn($array, $key)
    {
        if (!is_array($array)) {
            return [];
        }
        $arrayNew = [];
        foreach ($array as $value) {
            $arrayNew[$value[$key]] = $value;
        }
        return $arrayNew;
    }

    /**
     * xml转数组
     *
     * @date 2016年7月1日下午4:40:49
     *
     * @author lj
     * @param string $xml xml
     * @return return_type
     */
    public static function xmlToArray($xml)
    {
        libxml_disable_entity_loader(true);
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    }

    /**
     * 计算分页
     *
     * @param integer $pageNum 页面编码PageNum
     * @param integer $pageSize 页面大小PageSize
     * @param integer $offset 偏移量，默认0
     * @return integer startLimit
     */
    public static function getStartLimit($pageNum, $pageSize, $offset = 0)
    {
        $startLimit = ($pageNum - 1) * $pageSize;
        if ($startLimit < 0) {
            $startLimit = 0;
        }
        return $startLimit + $offset;
    }

    /**
     * 格式金额，默认保留2位小数
     *
     * @param $value
     * @param int $precision
     * @return float
     */
    public static function formatPrice($value, $precision = 2)
    {
        return round($value, $precision);
    }

    /**
     * 格式金额，默认保留2位小数
     *
     * @param $value
     * @param int $precision
     * @return float
     */
    public static function numberFormatPrice($value, $precision = 2)
    {
        return number_format($value, $precision);
    }

    /**
     * jsonEncode
     *
     * @param $value
     * @param int $options
     * @return string
     */
    public static function jsonEncode($value, $options = JSON_UNESCAPED_UNICODE)
    {
        return json_encode($value, $options);
    }

    /**
     * jsonDecode
     *
     * @param $value
     * @param boolean $assoc
     * @return array
     */
    public static function jsonDecode($value, $assoc = true)
    {
        return json_decode($value, $assoc);
    }

    /** 获取中文字符串首字母
     * @param $s0 中文字符串
     * @return string 首字母
     */
    public static function getFirstLetter($s0)
    {
        try {
            $firstchar_ord = ord(strtoupper($s0{0}));
            if ($firstchar_ord >= 65 and $firstchar_ord <= 91) {
                return strtoupper($s0{0});
            }

            if ($firstchar_ord >= 48 and $firstchar_ord <= 57) {
                return '#';
            }

            $s = iconv("UTF-8", "gb2312", $s0);
            $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
            if ($asc >= -20319 and $asc <= -20284) {
                return "A";
            }

            if ($asc >= -20283 and $asc <= -19776) {
                return "B";
            }

            if ($asc >= -19775 and $asc <= -19219) {
                return "C";
            }

            if ($asc >= -19218 and $asc <= -18711) {
                return "D";
            }

            if ($asc >= -18710 and $asc <= -18527) {
                return "E";
            }

            if ($asc >= -18526 and $asc <= -18240) {
                return "F";
            }

            if ($asc >= -18239 and $asc <= -17923) {
                return "G";
            }

            if ($asc >= -17922 and $asc <= -17418) {
                return "H";
            }

            if ($asc >= -17417 and $asc <= -16475) {
                return "J";
            }

            if ($asc >= -16474 and $asc <= -16213) {
                return "K";
            }

            if ($asc >= -16212 and $asc <= -15641) {
                return "L";
            }

            if ($asc >= -15640 and $asc <= -15166) {
                return "M";
            }

            if ($asc >= -15165 and $asc <= -14923) {
                return "N";
            }

            if ($asc >= -14922 and $asc <= -14915) {
                return "O";
            }

            if ($asc >= -14914 and $asc <= -14631) {
                return "P";
            }

            if ($asc >= -14630 and $asc <= -14150) {
                return "Q";
            }

            if ($asc >= -14149 and $asc <= -14091) {
                return "R";
            }

            if ($asc >= -14090 and $asc <= -13319) {
                return "S";
            }

            if ($asc >= -13318 and $asc <= -12839) {
                return "T";
            }

            if ($asc >= -12838 and $asc <= -12557) {
                return "W";
            }

            if ($asc >= -12556 and $asc <= -11848) {
                return "X";
            }

            if ($asc >= -11847 and $asc <= -11056) {
                return "Y";
            }

            if ($asc >= -11055 and $asc <= -10247) {
                return "Z";
            }
        } catch (\Exception $e) {

        }

        return '#';
    }

    /**
     * 解码base64加密的图片
     *
     * @param string $imgData
     * @param bool $decode
     * @return string
     */
    public static function base64DecodeImg($imgData, $decode = true)
    {
        preg_match('/^(data:\s*image\/(\w+);base64,)/', $imgData, $result);
        if ($decode) {
            return base64_decode(str_replace(' ', '+', str_replace($result[1], '', $imgData)));
        } else {
            if (count($result) > 1) {
                return str_replace(' ', '+', str_replace($result[1], '', $imgData));
            } else {
                return $imgData;
            }
        }
    }

    /**
     * 货币金额
     *
     * @param $amount
     * @param $rate
     * @return float
     */
    public static function currencyAmount($amount, $rate)
    {
        if ($rate <= 0) {
            return $amount;
        }
        return self::formatPrice($amount * $rate, 4);
    }

    /**
     * 解析版本号
     *
     * @param $versionStr
     * @return integer
     */
    public static function parseVersion($versionStr)
    {
        $version = explode('.', $versionStr);
        if (count($version) == 3) {
            $versionStr = $version[0] * 10000 + $version[1] * 100 + $version[2];
        }
        return intval($versionStr);
    }

    /**
     * 根据房门号,解析出楼层
     * @param $houseNo
     * @return string
     */
    public static function parseFloorNo($houseNo)
    {
        return substr($houseNo, 0, strlen($houseNo) - 2);
    }

    /**
     * 转换性别（文字转数字）
     * @param $sex
     * @return int|mixed
     */
    public static function sexNumber($sex)
    {
        $sexArray = [
            '男' => 1,
            '女' => 2
        ];
        return array_key_exists($sex, $sexArray) ? $sexArray[$sex] : 0;
    }

    /**
     * 手机号私密化
     *
     * @param string $mobile 手机号码
     * @return 手机号中间4位用*代替
     */
    public static function safeMobile($mobile)
    {
        return preg_replace('/(\d{3})\d{4}(\d{4})/', '$1****$2', $mobile);
    }

    /**
     * 截取字符串
     * @param string $str 字符串
     * @param integer 截取长度
     * @param string 补齐字符串
     * @return string
     */
    public static function getShort($str, $length = 40, $ext = '')
    {
        $str = htmlspecialchars($str);
        $str = strip_tags($str);
        $str = htmlspecialchars_decode($str);
        $strlenth = 0;
        $output = '';
        preg_match_all("/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/", $str, $match);
        foreach ($match[0] as $v) {
            preg_match("/[\xe0-\xef][\x80-\xbf]{2}/", $v, $matchs);
            if (!empty($matchs[0])) {
                $strlenth += 1;
            } elseif (is_numeric($v)) {
                //$strlenth +=  0.545;  // 字符像素宽度比例 汉字为1
                $strlenth += 0.5; // 字符字节长度比例 汉字为1
            } else {
                //$strlenth +=  0.475;  // 字符像素宽度比例 汉字为1
                $strlenth += 0.5; // 字符字节长度比例 汉字为1
            }

            if ($strlenth > $length) {
                $output .= $ext;
                break;
            }

            $output .= $v;
        }
        return $output;
    }

    /**
     * 判断是否工作时间
     * @return bool
     */
    public static function isWorkingTime()
    {
        $now = Carbon::now();
        if ($now->isWeekend()) {
            // 周末
            return false;
        }
        $start = Carbon::create($now->year, $now->month, $now->day, 9, 30, 0); // 上班时间
        $end = Carbon::create($now->year, $now->month, $now->day, 18, 00, 0); // 下班时间
        return $now->between($start, $end);
    }

    /**
     * 旋转七牛云图片
     *
     * @param string $imgUrl 图片链接
     * @param integer n$rotate 旋转角度 1-360
     * @return string
     */
    public static function imageRotate($imgUrl, $rotate)
    {
        if ($rotate > 0 && preg_match("/(crm.cdn.unionblue.cn|pav0uky18.bkt.clouddn.com|ihomejoy-jiangjin.leinpay.com)/", $imgUrl)) {
            $matches = [];
            if (preg_match("/(\?imageMogr2.*\/rotate\/)(\d+)/", $imgUrl, $matches)) {
                $imgUrl = preg_replace("/(\?imageMogr2.*\/rotate\/)(\d+)/", $matches[1] . $rotate, $imgUrl);
            } else if (preg_match("/\?imageMogr2/", $imgUrl, $matches)) {
                $imgUrl .= "/rotate/" . $rotate;
            } else {
                $imgUrl .= "?imageMogr2/rotate/" . $rotate;
            }
        }
        return $imgUrl;
    }

    /**
     * 递归方式遍历目录
     * @param string $dir
     * @return array
     */
    public static function read_dir_recursive($dir)
    {
        $files = array();
        $dir_list = scandir($dir);
        foreach ($dir_list as $file) {
            if ($file != '..' && $file != '.') {
                if (is_dir($dir . '/' . $file)) {
                    $files[] = self::read_dir_recursive($dir . '/' . $file);
                } else {
                    $files[] = $file;
                }
            }
        }
        return $files;
    }

    /**
     * 队列方式 遍历目录
     * @param string $dir
     * @return array
     */
    public static function read_dir_queue($dir)
    {
        $files = array();
        $queue = array($dir);
        while ($data = each($queue)) {
            $path = $data['value'];
            if (is_dir($path) && $handle = opendir($path)) {
                while ($file = readdir($handle)) {
                    if ($file == '.' || $file == '..') continue;
                    $files[] = $real_path = $path . '/' . $file;
                    if (is_dir($real_path)) $queue[] = $real_path;
                }
            }
            closedir($handle);
        }
        return $files;
    }

    /**
     * UTF8字符串截取函数
     * @param $str
     * @param $len
     * @return string
     */
    public static function utf8sub($str, $len, $offset = 0)
    {
        if ($len < 0) {
            return '';
        }
        $res = '';
        // $offset = 0;
        $chars = 0;
        $count = 0;
        $length = strlen($str);//待截取字符串的字节数
        while ($chars < $len && $offset < $length) {
            $high = decbin(ord(substr($str, $offset, 1)));//先截取客串的一个字节，substr按字节进行截取
            //重要突破，已经能够判断高位字节
            if (strlen($high) < 8) {//英文字符ascii编码长度为7，通过长度小于8来判断
                $count = 1;
                // echo 'hello,I am in','<br>';
            } elseif (substr($high, 0, 3) == '110') {
                $count = 2;    //取两个字节的长度
            } elseif (substr($high, 0, 4) == '1110') {
                $count = 3;    //取三个字节的长度
            } elseif (substr($high, 0, 5) == '11110') {
                $count = 4;

            } elseif (substr($high, 0, 6) == '111110') {
                $count = 5;
            } elseif (substr($high, 0, 7) == '1111110') {
                $count = 6;
            }
            $res .= substr($str, $offset, $count);
            $chars += 1;
            $offset += $count;
        }
        return $res;
    }

    /**
     * 验证姓名是否为复姓
     * @param $userName
     * @return boolean
     */
    public static function checkDoubleName($userName)
    {
        $double_array = array("万俟", "司马", "上官", "欧阳", "夏侯", "诸葛", "闻人", "东方", "赫连", "皇甫", "尉迟", "公羊",
            "澹台", "公冶", "宗政", "濮阳", "淳于", "单于", "太叔", "申屠", "公孙", "仲孙", "轩辕", "令狐",
            "锺离", "宇文", "长孙", "慕容", "鲜于", "闾丘", "司徒", "司空", "丌官", "司寇", "子车", "微生",
            "颛孙", "端木", "巫马", "公西", "漆雕", "乐正", "壤驷", "公良", "拓拔", "夹谷", "宰父", "谷梁",
            "段干", "百里", "东郭", "南门", "呼延", "羊舌", "梁丘", "左丘", "东门", "西门", "南宫"
        );

        $double_name = self::utf8sub($userName, 2);
        if (in_array($double_name, $double_array)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * 截取姓
     * @param $userName
     * @return boolean
     */
    public static function getFamilyName($userName)
    {
        if (self::checkDoubleName($userName)) {
            return self::utf8sub($userName, 2);
        } else {
            return self::utf8sub($userName, 1);
        }
    }

    /**
     * 转换姓名为姓+称呼
     * @param $userName
     * @param $sex
     * @return string
     */
    public static function changeToPrivacyName($userName, $sex)
    {
        $privacyName = self::getFamilyName($userName);
        $privacyName .= $sex == 1 ? '先生' : '女士';
        return $privacyName;
    }

    /**
     * 获取日期区间数组
     * @param $startDate
     * @param $endDate
     * @return array
     */
    public static function createdDateRange($startDate, $endDate)
    {
        $_time = range(strtotime($startDate), strtotime($endDate) - 24 * 60 * 60, 24 * 60 * 60);

        $_time = array_map(create_function('$v', 'return date("Y-m-d", $v);'), $_time);

        return $_time;
    }

    /**
     * 获取当前毫秒数
     * @return float
     */
    public static function getMillisecond()
    {
        list($s1, $s2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }


    /**
     * 数字转字母 （类似于Excel列标）
     * @param Int $index 索引值
     * @param Int $start 字母起始值
     * @return String 返回字母
     */
    public static function IntToChr($index, $start = 65)
    {
        $str = '';
        if (floor($index / 26) > 0) {
            $str .= self::IntToChr(floor($index / 26) - 1);
        }
        return $str . chr($index % 26 + $start);
    }

    /**
     * 创建多级目录
     * @param $dir
     * @return bool
     */
    function mkdirs($dir)
    {
        if (!is_dir($dir)) {
            if (!mkdirs(dirname($dir))) {
                return false;
            }
            if (!mkdir($dir, 0777)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 下载文件到本地指定路径
     * @param $fileUrl
     * @param $saveTo
     * @return bool
     */
    public static function downloadFile($fileUrl, $saveTo)
    {
        $content = file_get_contents($fileUrl);
        return  self::mkdirs(dirname($saveTo)) === false || file_put_contents($saveTo, $content) === false ? false : true;
    }

    /**
     * 获取七牛查询key
     * @param $fullUrl
     * @return null|string|string[]
     */
    public static function getQiniuKey($fullUrl)
    {
        return preg_replace('/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+\//i', '', $fullUrl);
    }

    /**
     * 对象转数组
     * @param $object
     * @return mixed
     */
    public static function objectToArray($object) {
        //先编码成json字符串，再解码成数组
        return json_decode(json_encode($object), true);
    }

    /**
     * 检查是否合法json
     * @param $string
     * @return bool
     */
    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
