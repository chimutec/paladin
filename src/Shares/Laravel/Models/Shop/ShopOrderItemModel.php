<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-09 17:12
 */

namespace Paladin\Shares\Laravel\Models\Shop;


use Paladin\Shares\Laravel\Models\BaseModel;
use Paladin\Shares\Laravel\Providers\Helper\Helper;

class ShopOrderItemModel extends BaseModel
{
    protected $table = "shop_order_item";

    /**
     * 状态: 待支付
     */
    const STATUS_PENDING = 0;

    /**
     * 状态: 已支付
     */
    const STATUS_PURCHASED = 10;

    /**
     * 状态: 已完成
     */
    const STATUS_COMPLETE = 20;

    public static $quotaStatusList = [
        self::STATUS_PURCHASED,
        self::STATUS_COMPLETE,
    ];

    public static $statusTextList = [
        self::STATUS_PENDING => '待支付',
        self::STATUS_PURCHASED => '已支付',
        self::STATUS_COMPLETE => '已完成',
    ];

    public function getGoodsMainImgAttribute($value)
    {
        return Helper::fullPicPath($value);
    }

    public function getGoodsSkuPriceAttribute($key)
    {
        return intval($key);
    }
}