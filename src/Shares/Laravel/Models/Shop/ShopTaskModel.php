<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-08 20:03
 */

namespace Paladin\Shares\Laravel\Models\Shop;


use Illuminate\Database\Eloquent\SoftDeletes;
use Paladin\Shares\Laravel\Models\BaseModel;
use Paladin\Shares\Laravel\Providers\Helper\Helper;

class ShopTaskModel extends BaseModel
{
    use SoftDeletes;

    protected $table = "shop_task";

    /**
     * 任务类型: 新人任务
     * @var string
     */
    const TYPE_TIRO = "tiro";

    /**
     * 任务类型: 日常任务
     * @val string
     */
    const TYPE_DAILY = "daily";

    public static $typeList = [
        self::TYPE_TIRO,
        self::TYPE_DAILY,
    ];

    public static $typeTextList = [
        self::TYPE_TIRO => '新人',
        self::TYPE_DAILY => '日常',
    ];

    /**
     * 任务目标类型: pad
     */
    const TARGET_TYPE_PAD = "pad";

    const TARGET_TYPE_MINIA_APP = "minia_app";

    public static $targetTypeList = [
        self::TARGET_TYPE_PAD,
        self::TARGET_TYPE_MINIA_APP,
    ];

    public static $targetTypeTextList = [
        self::TARGET_TYPE_PAD => '设备',
        self::TARGET_TYPE_MINIA_APP => '微信小程序',
    ];

    public function getIconAttribute($value)
    {
        return Helper::fullPicPath($value);
    }

    public function getIntegralAttribute($key)
    {
        return intval($key);
    }
}