<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-09 16:16
 */

namespace Paladin\Shares\Laravel\Models\Shop;

use Illuminate\Database\Eloquent\SoftDeletes;
use Paladin\Shares\Laravel\Models\BaseModel;

class ShopGoodsSkuModel extends BaseModel
{
    use SoftDeletes;

    protected $table = "shop_goods_sku";

    public function getSkuPriceAttribute($key)
    {
        return intval($key);
    }
}