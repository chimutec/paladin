<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-09 15:36
 */

namespace Paladin\Shares\Laravel\Models\Shop;

use Illuminate\Database\Eloquent\SoftDeletes;
use Paladin\Shares\Laravel\Models\BaseModel;
use Paladin\Shares\Laravel\Providers\Helper\Helper;

class ShopGoodsModel extends BaseModel
{
    use SoftDeletes;

    protected $table = "shop_goods";


    public function getPriceAttribute($key)
    {
        return intval($key);
    }

    /**
     * 格式图片url
     * @param $value
     * @return string
     */
    public function getMainImgAttribute($value)
    {
        return Helper::fullPicPath($value);
    }
}