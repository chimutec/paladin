<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-11 21:06
 */

namespace Paladin\Shares\Laravel\Models\Shop;

use Paladin\Shares\Laravel\Models\BaseModel;

class ShopMemberIntegralModel extends BaseModel
{
    protected $table = "shop_member_integral";

    /**
     * 类型: 收入
     */
    const TYPE_IN = 'in';

    /**
     * 类型: 支出
     */
    const TYPE_OUT = "out";

    public static $typeTextList = [
        self::TYPE_IN => '收入',
        self::TYPE_OUT => '支出',
    ];

    /**
     * 关联模型: 订单
     */
    const APP_TYPE_ORDER = "order";

    /**
     * 关联模型: 任务
     */
    const APP_TYPE_TASK = "task";
}