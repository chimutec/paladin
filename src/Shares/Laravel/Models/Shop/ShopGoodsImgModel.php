<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-09 16:07
 */

namespace Paladin\Shares\Laravel\Models\Shop;

use Paladin\Shares\Laravel\Models\BaseModel;
use Paladin\Shares\Laravel\Providers\Helper\Helper;

class ShopGoodsImgModel extends BaseModel
{
    protected $table = "shop_goods_img";

    /**
     * 格式图片url
     * @param $value
     * @return string
     */
    public function getImgUrlAttribute($value)
    {
        return Helper::fullPicPath($value);
    }
}