<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-11 20:06
 */

namespace Paladin\Shares\Laravel\Models\Shop;

use Illuminate\Database\Eloquent\SoftDeletes;
use Paladin\Shares\Laravel\Models\BaseModel;

class ShopOrderModel extends BaseModel
{
    use SoftDeletes;

    protected $table = "shop_order";

    /**
     * 等待付款
     * @var int
     */
    const STATUS_WAIT_PAY = 0;

    /**
     * 付款取消
     *
     * @var int
     */
    const STATUS_TIMES_EXPIRES = 10;

    /**
     * 支付失败 请联系管理员
     * @var int
     */
    const STATUS_PAY_FAIL = 20;

    /**
     * 付款成功 待发货 回调成功时状态
     * @var int
     */
    const STATUS_PAY_SUCCESS = 30;

    /**
     * 已发货
     * @var int
     */
    const STATUS_HAS_DELIVER = 40;

    /**
     * 已收货
     * @var int
     */
    const STATUS_HAS_RECEIPT = 50;

    public static $statusTextList = [
        self::STATUS_WAIT_PAY => '待支付',
        self::STATUS_TIMES_EXPIRES => '已关闭',
        self::STATUS_PAY_FAIL => '支付失败',
        self::STATUS_PAY_SUCCESS => '支付成功',
        self::STATUS_HAS_DELIVER => '已发货',
        self::STATUS_HAS_RECEIPT => '完成',
    ];

    public function getPaymentAmountAttribute($key)
    {
        return intval($key);
    }
}