<?php
/**
 * Created by PhpStorm.
 * User: zhaolin
 * Email: 79534505@qq.com
 * Date Time: 2019-08-08 20:27
 */

namespace Paladin\Shares\Laravel\Models\Shop;

use Paladin\Shares\Laravel\Models\BaseModel;

class ShopMemberTaskModel extends BaseModel
{
    protected $table = "shop_member_task";

    /**
     * 用户默认积分
     * @val int
     */
    const DEFAULT_INTEGRAL = 0;
}