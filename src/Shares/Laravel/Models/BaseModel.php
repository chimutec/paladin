<?php

namespace Paladin\Shares\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BaseModel extends Model
{

    protected $_where = null;
    protected $_order = null;
    protected $_group = null;
    protected $_select = null;
    protected $_carbonField = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * 真
     * @var integer
     */
    const IS_TRUE = 1;

    /**
     * 假
     * @var integer
     */
    const IS_FALSE = 0;

    /**
     * 平台：Android
     * @var string
     */
    const PLATFORM_ANDROID = 'android';

    /**
     * 平台：ios
     * @var string
     */
    const PLATFORM_IOS = 'ios';

    /**
     * none
     * @var string
     */
    const APP_TABLE_NONE = 'none';

    /**
     * member
     * @var string
     */
    const APP_TABLE_MEMBER = 'member';

    /**
     * 系统管理员
     * @var string
     */
    const APP_TABLE_ADMIN = 'admin';

    /**
     * 网格园管理员
     * @var string
     */
    const APP_TABLE_GRID_PERSON = 'grid_person';

    /**
     * 网格园
     * @var string
     */
    const APP_TABLE_GRID = 'grid';

    /**
     * 民警
     * @var string
     */
    const APP_TABLE_POLICE = 'police';

    /**
     * 派出所
     * @var string
     */
    const APP_TABLE_POLICE_STATION = 'police_station';

    /**
     * 小区
     * @var string
     */
    const APP_TABLE_ESTATE = 'estate';

    /**
     * 楼宇
     * @var string
     */
    const APP_TABLE_BUILDING = 'building';

    /**
     * 物业人员
     * @var string
     */
    const APP_TABLE_ESTATE_SERVICE_PERSON = 'estate_service_person';

    /**
     * 物业
     * @var string
     */
    const APP_TABLE_ESTATE_SERVICE = 'estate_service';

    /**
     * 贵宾
     * @var string
     */
    const APP_TABLE_VIP_PERSON = 'vip_person';

    /**
     * 设备
     * @var string
     */
    const APP_TABLE_DEVICE = 'device';

    /**
     * 设备密码
     * @var string
     */
    const APP_TABLE_DEVICE_PASSWORD = 'device_password';

    /**
     * 人物信息
     * @var string
     */
    const APP_TABLE_PERSON = 'person';

    /**
     * 房屋关系
     * @var string
     */
    const APP_TABLE_ROOM_PERSON = 'room_person';

    /**
     * id卡
     * @var string
     */
    const APP_TABLE_IDCARD = 'idcard';

    /**
     * 房屋
     * @var string
     */
    const APP_TABLE_ROOM = 'room';

    /**
     * 短租房房屋
     * @var string
     */
    const APP_TABLE_RENT_ROOM = 'rent_room';

    /**
     * 短租房房屋关系
     * @var string
     */
    const APP_TABLE_RENT_ROOM_PERSON = 'rent_room_person';

    /**
     * 访客关系
     * @var string
     */
    const APP_TABLE_ROOM_VISITOR = 'room_visitor';

    /**
     * 短租房订单
     * @var string
     */
    const APP_TABLE_RENT_ORDER = 'rent_order';

    /**
     * 缴费订单
     * @var string
     */
    const APP_TABLE_COST = 'cost';

    /**
     * 退款表
     * @var string
     */
    const APP_TABLE_REFUND = 'refund';


    /**
     * 对应APP:智悦优家
     */
    const APP_TYPE_IHOMEJOY = 'ihomejoy';

    /**
     * 对应APP:短租房
     */
    const APP_TYPE_RENT = 'rent';

    /**
     * 对应APP: 商城
     */
    const APP_TYPE_MALL = 'mall';

    public static $deviceAppTextList = [
        self::APP_TYPE_IHOMEJOY => '智悦优家',
        self::APP_TYPE_RENT => '优家小住'
    ];

    /**
     * 封装插入函数
     *
     * @param array $attributes 插入属性
     * @param array $fillable
     * @return int 自增id
     */
    public static function insert(array $attributes, $fillable = [])
    {
        $model = new static();
        $model->fillable(empty($fillable) ? array_keys($attributes) : $fillable);
        $model->fill($attributes);
        if ($model->save()) {
            return $model->getKey();
        } else {
            return 0;
        }
    }

    /**
     * 用来设置分页查询where条件，设置$_where字段
     *
     * @param array $where key：字段名+操作符（如"status in"） value:字段值（如"20,30,40"）
     * @param string $type 连接符
     * @return BaseModel
     */
    public function setWhere($where = null, $type = 'and')
    {
        if (empty($where) || !is_array($where))
            return $this;

        $data = array();
        $data['bind'] = array();
        $searchString = '';

        foreach ($where as $search => $value) {
            $isIn = strstr($search, ' in');
            $isLike = strstr($search, ' like');

            try {
                $null = strstr($value, 'null');
            } catch (\Exception $e) {
                $null = false;
            }

            if ($isIn) {
                if (is_array($value)) {
                    $value = "('" . implode("','", $value) . "')";
                } else {
                    $value = "($value)";
                }
                $searchString .= ' ' . trim($search) . " $value " . $type;
            } elseif ($isLike) {
                $searchString .= ' ' . trim($search) . " " . $type;
                $data['bind'][] = trim($value);
            } elseif ($null) {
                $searchString .= ' ' . trim($search) . " $value " . $type;
            } else {
                $searchString .= ' ' . trim($search) . ' = ? ' . $type;
                $data['bind'][] = trim($value);
            }
        }

        $data['param'] = substr_replace(trim($searchString, ' '), '', -4); // 去掉最后的 " and"
        $this->_where = $data;
        return $this;
    }

    /**
     * 用来设置分页查询排序条件，设置$this->_order字段;
     *
     * @param array $order key:字段名 value:排序方式（desc, asc）
     * @return BaseModel
     */
    public function setOrder($order = null)
    {
        if (empty($order) || !is_array($order)) {
            return $this;
        }

        $orderString = '';
        foreach ($order as $field => $type) {
            $orderString .= $field . ' ' . $type . ',';
        }

        $this->_order = trim($orderString . $this->_order, ',');
        return $this;
    }

    /**
     * 用来设置分组查询group条件，设置$_group字段
     *
     * @param $group
     * @return $this
     */
    public function setGroup($group)
    {
        if (empty($group)) {
            return $this;
        }

        $this->_group = $group;
        return $this;
    }

    /**
     * 用来设置查询select条件，设置$_select字段
     *
     * @param $select
     * @return $this
     */
    public function setSelect($select)
    {
        if (empty($select) || !is_array($select)) {
            return $this;
        }

        $this->_select = $select;
        return $this;
    }

    /**
     * 利用$_where和$_order进行查询
     *
     * @param int $page 页号
     * @param int $limit 每页条数
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getListByPage($page = 0, $limit = 3)
    {
        $obj = $this;
        // SELECT
        if (!empty($this->_select)) {
            $obj = $obj->select($this->_select);
        }

        // 注册搜索条件
        if (!empty($this->_where)) {
            $obj = $obj->whereRaw($this->_where['param'], $this->_where['bind']);
        }

        // SQL分组
        if (!empty($this->_group)) {
            $obj = $obj->groupBy(DB::raw($this->_group));
        }

        $obj = $obj->offset($page)->limit($limit);
        if (!empty($this->_order)) {
            return $obj->orderByRaw($this->_order)->get();
        } else {
            return $obj->get();
        }
    }

    /**
     * 获取查询条件
     *
     * @return
     */
    public function getWhere()
    {
        return $this->_where;
    }

    /**
     * 获取排序条件
     *
     * @return
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * 获取分组
     *
     * @return
     */
    public function getGroupy()
    {
        return $this->_group;
    }

    /**
     * 获取select
     *
     * @return
     */
    public function getSelect()
    {
        return $this->_select;
    }

    /**
     * 用来计算查询出的总条数
     *
     * @return int
     */
    public function getCount()
    {
        $obj = $this;
        if (!empty($this->_where)) {
            $obj = $obj->whereRaw($this->_where['param'], $this->_where['bind']);
        }

        if (!empty($this->_group)) {
            return count($obj->select($this->primaryKey)->groupBy(DB::raw($this->_group))->get());
        }

        return $obj->count();
    }

    /**
     * 格式化删除参数
     * @param $value
     * @return bool
     */
    public function getIsDelAttribute($value)
    {
        return boolval($value);
    }
}
