<?php

namespace Paladin\Shares\Laravel\Models;

class ConfigModel extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    public $fillable = ['config_value'];
}