<?php

namespace Paladin\Shares\Laravel\Models;

class BaseLogModel extends BaseModel
{
    /**
     * 制定链接数据库
     * @var string
     */
    protected $connection = 'dblog';
}