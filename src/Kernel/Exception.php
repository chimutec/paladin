<?php

namespace Paladin\Kernel;

use Throwable;

class Exception extends \Exception
{
    protected $data = [];

    public function __construct($message = "", $code = 0, Throwable $previous = null, $data=[])
    {
        parent::__construct($message, $code, $previous);
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}