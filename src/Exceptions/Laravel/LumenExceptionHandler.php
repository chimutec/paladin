<?php
namespace Paladin\Exceptions\Laravel;

use Exception;
use App\Exceptions\Extensions\AppException;
use App\Exceptions\Extensions\ConflictException;
use App\Exceptions\Extensions\NetworkException;
use App\Exceptions\Extensions\NotFoundException;
use App\Exceptions\Extensions\ParseException;
use App\Exceptions\Extensions\SysException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler;
use Illuminate\Support\Facades\Log;
use Paladin\Shares\Laravel\Config\BaseGlobalConstant;
use Paladin\Shares\Laravel\Services\LogErrorService;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;

class LumenExceptionHandler extends Handler
{
    public function report(Exception $e)
    {
        // 一般的自定义exception
        if ($e instanceof ModelNotFoundException) throw new NotFoundHttpException($e->getMessage(), $e);
        if ($e instanceof NotFoundException) throw new NotFoundHttpException($e->getMessage(), $e);
        if ($e instanceof ConflictException) throw new ConflictHttpException($e->getMessage(), $e);
        if ($e instanceof ParseException) throw new UnprocessableEntityHttpException($e->getMessage(), $e);

        // 自定义exception基类
        if ($e instanceof AppException) throw new UnprocessableEntityHttpException($e->getMessage(), $e);
        if ($e instanceof NetworkException) throw new UnprocessableEntityHttpException($e->getMessage(), $e);
        if ($e instanceof SysException) throw new UnprocessableEntityHttpException($e->getMessage(), $e);

        // HTTP Exception按正常流程处理
        if ($e instanceof HttpException) return parent::report($e);
    }

    /**
     * 处理日志
     * @param Exception $e
     * @throws Exception
     */
    private function logHandle(Exception $e)
    {
        try {
            $logger = app('Psr\Log\LoggerInterface');
        } catch (Exception $ex) {
            throw $e; // throw the original exception
        }

        // todo 以后记录到日志系统
        $logger->error($e, ['exception' => $e]);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof HttpResponseException) {
            return $exception->getResponse();
        } elseif ($exception instanceof ModelNotFoundException) {
            $exception = new NotFoundHttpException($exception->getMessage(), $exception);
        } elseif ($exception instanceof AuthorizationException) {
            $exception = new HttpException(403, $exception->getMessage(), $exception);
        } elseif ($exception instanceof ValidationException && $exception->getResponse()) {
            return $exception->getResponse();
        } elseif ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json(LogErrorService::logErrorCode(BaseGlobalConstant::ERROR_INVALID_ROUTE), 404);
        }

        // 记录日志
        $this->logHandle($exception);
        // 线上做特殊处理
        if (app()->environment('production')) {
            return response()->json(LogErrorService::logErrorCode(BaseGlobalConstant::ERROR_SYSTEM), 500);
        }
        if (preg_match('/[0-9]+\.[0-9]+\.[0-9]+/is', app()->version(), $matches)) {
            $version = (int)str_replace('.', '', $matches[0]);
            // 检查框架版本 lumen 5.1.* 以下的版本异常输出不一致
            if ($version <= 520) {
                $response = (new SymfonyExceptionHandler(env('APP_DEBUG', false)))->createResponse($exception);
                return $this->toIlluminateResponse($response, $exception);
            }
        }

        $fe = FlattenException::create($exception);

        $handler = new SymfonyExceptionHandler(env('APP_DEBUG', config('app.debug', false)));

        $decorated = $this->decorate($handler->getContent($fe), $handler->getStylesheet($fe));

        $response = new Response($decorated, $fe->getStatusCode(), $fe->getHeaders());

        $response->exception = $exception;

        return $response;
    }
}